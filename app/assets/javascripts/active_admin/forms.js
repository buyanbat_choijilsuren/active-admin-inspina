//= require ./ext/iCheck/icheck.min.js
//= require ./ext/staps/jquery.steps.min.js
//= require ./ext/validate/jquery.validate.min.js
//= require ./ext/dropzone/dropzone.js
//= require ./ext/summernote/summernote.min.js
//= require ./ext/colorpicker/bootstrap-colorpicker.min.js
//= require ./ext/cropper/cropper.min.js
//= require ./ext/datapicker/bootstrap-datetimepicker.min.js
//= require ./ext/ionRangeSlider/ion.rangeSlider.min.js
//= require ./ext/jasny/jasny-bootstrap.min.js
//= require ./ext/jsKnob/jquery.knob.js
//= require ./ext/nouslider/jquery.nouislider.min.js
//= require ./ext/switchery/switchery.js
//= require ./ext/chosen/chosen.jquery.js
//= require ./ext/fullcalendar/moment.min.js
//= require ./ext/fullcalendar/fullcalendar.min.js
//= require ./ext/toastr/toastr.min.js




$(document).ready(function () {
	$("select.chosen-select").chosen();
	$("textarea.summernote").summernote();
	$("input.switcher").each(function( index ) {
		new Switchery(this, {color: "#1AB394"});
	});

	$(".i-checks").iCheck({
		checkboxClass: 'icheckbox_square-green',
		radioClass: 'iradio_square-green'
	});

	toastr.options = {
		closeButton: true,
		progressBar: true,
		positionClass: "toast-top-center m-t"
	};

	if($(".flash_alert").length > 0){
		toastr.warning($(".flash_alert").text(), "Alert");
	}

	if($(".flash_notice").length > 0){
		toastr.success($(".flash_notice").text(), "Info");
	}
});
