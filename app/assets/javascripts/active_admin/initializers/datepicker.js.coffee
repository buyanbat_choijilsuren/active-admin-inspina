$ -> 
	$('.input-group.date_only').datetimepicker({
	  calendarWeeks: true,
	  format: "yyyy-mm-dd",
	  autoclose: true,
	  minView: 2
  })

	$('.input-group.date_time').datetimepicker({
	  calendarWeeks: true,
	  autoclose: true
  })

	$('.form-group.filter_date_range input.datepicker').datetimepicker({
	  calendarWeeks: true,
	  autoclose: true,
	  format: "yyyy-mm-dd",
	  minView: 2
  })
