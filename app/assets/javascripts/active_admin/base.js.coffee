#= require jquery
#= require jquery_ujs

#= require bootstrap-sprockets
#= require_self
#= require ./ext/metisMenu/jquery.metisMenu.js
#= require ./ext/chartjs/Chart.min.js

#= require ./ext/pace/pace.min.js
#= require ./ext/peity/jquery.peity.min.js
#= require ./ext/slimscroll/jquery.slimscroll.min.js
#= require ./inspinia.js
#= require ./forms.js
#= require_tree ./lib
#= require_tree ./initializers



window.ActiveAdmin = {}


$ ->
	$(document).on 'change', '.file-uploader', (e) ->
		e.preventDefault()
		reader = new FileReader()
		file = @.files[0]
		that = $(@).parent()[0]
		label = $(@).val().replace(/\\/g, '/').replace(/.*\//, '')
		file_name = that.lastChild
		$(file_name).attr('value', label)
