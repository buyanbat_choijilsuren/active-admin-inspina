# Provides an intuitive way to build has_many associated records in the same form.
module Formtastic
  module Inputs
    module Base
      def input_wrapping(&block)
        html = template.content_tag(:div,
          [template.capture(&block), error_html, hint_html].join("\n").html_safe,
          wrapper_html_options
        )
        template.concat(html) if template.output_buffer && template.assigns[:has_many_block]
        html
      end

      def wrapper_html_options
        opts = wrapper_html_options_raw
        opts[:class] = ["form-group",wrapper_classes].join(' ')
        opts[:id] = wrapper_dom_id unless opts.has_key? :id
        opts
      end

      def input_html_options
        {class: 'form-control'}.merge(super)
      end

      def label_html_options
        opts = super
        opts[:class] = 'control-label'
        opts
      end
    end
    class FileInput
      def label_html_options
        opts = super
        opts[:for] = input_html_options[:id]
        opts
      end

      def input_html_options
        super.merge({class: "file-uploader hide"})
      end
      def wrapper_html_options
        opts = super
        opts[:class] = opts[:class].remove("file")
        opts
      end

      def to_html
        input_wrapping do
          label_html << (
          template.content_tag :div, class: "input-group" do 
            template.content_tag(:span, template.content_tag(:label, template.content_tag(:i, "", class: "fa fa-folder-open") + " Browse", class: "btn btn-primary", for: input_html_options[:id]), class: "input-group-btn") <<
            builder.file_field(method, input_html_options) <<
            builder.text_field(method, class: "file_name form-control", name: nil, id: nil, )
          end)
        end
      end

    
    end

    class DatePickerInput
      def to_html
        input_wrapping do 
          label_html <<
          template.content_tag(:div, 
            template.content_tag(:span, template.content_tag(:i, "", class: 'fa fa-calendar'),
              class: 'input-group-addon') << builder.text_field(method, input_html_options), 
            class: 'input-group date date_only')
        end
      end

      def html_input_type
        "text"
      end

      def input_html_options
        super.merge({class: "form-control"})
      end
    end

    class DatetimePickerInput
      def to_html
        input_wrapping do 
          label_html <<
          template.content_tag(:div, 
            template.content_tag(:span, template.content_tag(:i, "", class: 'fa fa-calendar'),
              class: 'input-group-addon') << builder.text_field(method, input_html_options), 
            class: 'input-group date date_time')
        end
      end

      def html_input_type
        "text"
      end

      def input_html_options
        super.merge({class: "form-control"})
      end
    end

    class BooleanInput

      def label_html_options
        {
          :for => input_html_options[:id],
          :class => super[:class] # remove 'label' class
        }
      end
    end

  end


  module Actions
    module Base
      def wrapper(&block)
        template.content_tag(:span, 
          [template.capture(&block)].join("\n").html_safe,
          wrapper_html_options
        )
      end

      def wrapper_html_options
        {class: "m-r"}
      end
      def button_html
        { class: "btn btn-primary" }
      end
    end
  end
  module Helpers
    module FieldsetWrapper
      def field_set_and_list_wrapping(*args, &block) # @private
        contents = args[-1].is_a?(::Hash) ? '' : args.pop.flatten
        html_options = args.extract_options!

        html_options[:class] = "row"

        if block_given?
          contents = if template.respond_to?(:is_haml?) && template.is_haml?
            template.capture_haml(&block)
          else
            template.capture(&block)
          end
        end

        # Work-around for empty contents block
        contents ||= ""

        # Ruby 1.9: String#to_s behavior changed, need to make an explicit join.
        contents = contents.join if contents.respond_to?(:join)

        legend = field_set_legend(html_options)
        fieldset = template.content_tag(:div,
          legend.html_safe <<  contents.html_safe, html_options.except(:builder, :parent, :name)
        )

        fieldset
      end

      def field_set_legend(html_options)
        legend  = (html_options[:name] || '').to_s
        # only applying if String includes '%i' avoids argument error when $DEBUG is true
        legend %= parent_child_index(html_options[:parent]) if html_options[:parent] && legend.include?('%i')
        legend  = template.content_tag(:h3, template.content_tag(:span, legend.html_safe)) unless legend.blank?
        legend
      end
    end
  end
end




module ActiveAdmin
  class FormBuilder < ::Formtastic::FormBuilder
    self.input_namespaces = [::Object, ::ActiveAdmin::Inputs, ::Formtastic::Inputs]

    # TODO: remove both class finders after formtastic 4 (where it will be default)
    self.input_class_finder = ::Formtastic::InputClassFinder
    self.action_class_finder = ::Formtastic::ActionClassFinder
    self.default_inline_error_class = "inline-errors text-danger"

    def cancel_link(url = {action: "index"}, html_options = {}, li_attrs = {})
      li_attrs[:class] ||= "cancel"
      html_options[:class] ||= "btn btn-default"
      li_content = template.link_to I18n.t('active_admin.cancel'), url, html_options
      template.content_tag(:span, li_content, li_attrs)
    end

    def input_wrapping(&block)
      template.content_tag(:div,
        [template.capture(&block), error_html, hint_html].join("\n").html_safe,
        wrapper_html_options
      )
    end

    def wrapper_html_options
      opts = wrapper_html_options_raw
      opts[:class] = ["form-group",wrapper_classes].join(' ')
      opts[:id] = wrapper_dom_id unless opts.has_key? :id
      opts
    end


    attr_accessor :already_in_an_inputs_block

    def assoc_heading(assoc)
      object.class.reflect_on_association(assoc).klass.model_name.
        human(count: ::ActiveAdmin::Helpers::I18n::PLURAL_MANY_COUNT)
    end

    def has_many(assoc, options = {}, &block)
      # remove options that should not render as attributes
      custom_settings = :new_record, :allow_destroy, :heading, :sortable, :sortable_start
      builder_options = {new_record: true}.merge! options.slice  *custom_settings
      options         = {for: assoc      }.merge! options.except *custom_settings
      options[:class] = [options[:class], "inputs has_many_fields"].compact.join(' ')
      sortable_column = builder_options[:sortable]
      sortable_start  = builder_options.fetch(:sortable_start, 0)

      if sortable_column
        options[:for] = [assoc, sorted_children(assoc, sortable_column)]
      end

      html = "".html_safe

      html << template.capture do
        contents = "".html_safe
        form_block = proc do |has_many_form|
          index    = parent_child_index options[:parent] if options[:parent]
          block.call has_many_form, index
          template.concat has_many_actions(has_many_form, builder_options, "".html_safe)
        end
        
        template.assign(has_many_block: true)
        contents = without_wrapper { inputs(options, &form_block) } || "".html_safe

        if builder_options[:new_record]
          contents << js_for_has_many(assoc, form_block, template, builder_options[:new_record], options[:class])
        else
          contents
        end
      end

      tag = @already_in_an_inputs_block ? :div : :div
      html = template.content_tag(tag, html, class: "has_many_container #{assoc}", 'data-sortable' => sortable_column, 'data-sortable-start' => sortable_start)
      template.concat(html) if template.output_buffer
      html
    end

    protected

    def has_many_actions(has_many_form, builder_options, contents)
      if has_many_form.object.new_record?
        contents << template.content_tag(:div, class: "form-group col-sm-2") do
          template.content_tag(:label, "Actions ", class: "control-label") <<
          template.link_to(template.content_tag(:i, " ", class: "fa fa-times") << I18n.t('active_admin.has_many_remove'), "#", class: 'btn btn-danger btn-block has_many_remove')
        end
      elsif builder_options[:allow_destroy]
        has_many_form.input(:_destroy, as: :boolean,
                            wrapper_html: {class: 'has_many_delete'},
                            label: I18n.t('active_admin.has_many_delete'))
      end

      if builder_options[:sortable]
        has_many_form.input builder_options[:sortable], as: :hidden

        contents << template.content_tag(:li, class: 'handle') do
          "MOVE"
        end
      end

      contents
    end

    def sorted_children(assoc, column)
      object.public_send(assoc).sort_by do |o|
        attribute = o.public_send column
        [attribute.nil? ? Float::INFINITY : attribute, o.id || Float::INFINITY]
      end
    end

    private

    def without_wrapper
      is_being_wrapped = @already_in_an_inputs_block
      @already_in_an_inputs_block = false

      html = yield

      @already_in_an_inputs_block = is_being_wrapped
      html
    end

    # Capture the ADD JS
    def js_for_has_many(assoc, form_block, template, new_record, class_string)
      assoc_reflection = object.class.reflect_on_association assoc
      assoc_name       = assoc_reflection.klass.model_name
      placeholder      = "NEW_#{assoc_name.to_s.underscore.upcase.gsub(/\//, '_')}_RECORD"
      opts = {
        for: [assoc, assoc_reflection.klass.new],
        class: class_string,
        for_options: { child_index: placeholder }
      }
      html = template.capture{ inputs_for_nested_attributes opts, &form_block }
      text = new_record.is_a?(String) ? new_record : I18n.t('active_admin.has_many_new', model: assoc_name.human)

      template.link_to text, '#', class: "btn btn-primary has_many_add btn-xs", data: {
        html: CGI.escapeHTML(html).html_safe, placeholder: placeholder
      }
    end
  end
  class HasManyInputsProxy < ActiveAdmin::Views::FormtasticProxy
    def build(*args, &block)
      html_options = args.extract_options!
      html_options[:class] ||= "ibox float-e-margins"
      legend = args.shift if args.first.is_a?(::String)
      legend = html_options.delete(:name) if html_options.key?(:name)
      legend_tag = legend ? "<div class='ibox-title'><h3>#{legend}</h3></div>" : ""
      fieldset_attrs = html_options.map {|k,v| %Q{#{k}="#{v}"} }.join(" ")
      @opening_tag = "<div #{fieldset_attrs}>#{legend_tag}<div class='ibox-content'>"
      @closing_tag = "</div></div>"
      super(*(args << html_options), &block)
    end
  end
end
