module ActiveAdmin
  module Inputs
    module Filters
      module Base
        include ::Formtastic::Inputs::Base
        include ::ActiveAdmin::Filters::FormtasticAddons

        extend ::ActiveSupport::Autoload
        autoload :SearchMethodSelect

        def input_wrapping(&block)
          template.content_tag :div, wrapper_html_options do 
            template.capture(&block)
          end  
        end

        def required?
          false
        end

        # Can pass proc to filter label option
        def label_from_options
          res = super
          res = res.call if res.is_a? Proc
          res
        end

        def label_html
          template.content_tag :div, class: "col-sm-12" do
            super
          end
        end

        def wrapper_html_options
          opts = super
          (opts[:class] ||= '') << " filter_form_field filter_#{as} form-group col-sm-12"
          opts
        end

        # Override the standard finder to accept a proc
        def collection_from_options
          if options[:collection].is_a?(Proc)
            template.instance_exec(&options[:collection])
          else
            super
          end
        end
        
        def input_html_options
          {class: "form-control"}
        end

        def label_html_options
          {class: 'control-label'}
        end
      end
    end
  end
end
