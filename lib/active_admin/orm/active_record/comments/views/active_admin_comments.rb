require 'active_admin/views'
require 'active_admin/views/components/panel'

module ActiveAdmin
  module Comments
    module Views

      class Comments < ActiveAdmin::Views::Panel
        builder_method :active_admin_comments_for

        attr_accessor :resource

        def build(resource)
          @resource = resource
          @comments = ActiveAdmin::Comment.find_for_resource_in_namespace resource, active_admin_namespace.name
          super(title, for: resource)
          build_comments
        end

        protected

        def title
          I18n.t 'active_admin.comments.title_content', count: @comments.count
        end

        def build_comments
          div class: :row do 
            div class: "col-sm-7" do 
              if @comments.any? 
                div class: "chat-discussion" do 
                  @comments.each(&method(:build_comment)) 
                end
              else 
                build_empty_message
              end
            end
            div class: "col-sm-5" do 
              build_comment_form
            end
          end
        end

        def build_comment(comment)
          div for: comment, class: "chat-message" do
            if comment.author.photo 
              text_node image_tag comment.author.photo.url(:small), class: "message-avatar"
            else
              text_node image_tag "user-default.png", class: "message-avatar"
            end
            div class: 'message' do
              a href: admin_admin_user_path(comment.author), class: "message-author" do 
                comment.author.name
              end
              span pretty_format(comment.created_at), class: "message-date"
              #if authorized?(ActiveAdmin::Auth::DESTROY, comment)
              #  text_node link_to I18n.t('active_admin.comments.delete'), comments_url(comment.id), method: :delete, data: { confirm: I18n.t('active_admin.comments.delete_confirmation') }
              #end
              span class: 'message-content' do
                simple_format comment.body
              end
            end
          end
        end

        def build_empty_message
          span I18n.t('active_admin.comments.no_comments_yet'), class: 'empty'
        end

        def comments_url(*args)
          parts = []
          parts << active_admin_namespace.name unless active_admin_namespace.root?
          parts << active_admin_namespace.comments_registration_name.underscore
          parts << 'path'
          send parts.join('_'), *args
        end

        def comment_form_url
          parts = []
          parts << active_admin_namespace.name unless active_admin_namespace.root?
          parts << active_admin_namespace.comments_registration_name.underscore.pluralize
          parts << 'path'
          send parts.join '_'
        end

        def build_comment_form
          active_admin_form_for(ActiveAdmin::Comment.new, url: comment_form_url) do |f|
            f.inputs do
              f.input :resource_type, as: :hidden,  input_html: { value: ActiveAdmin::Comment.resource_type(parent.resource) }
              f.input :resource_id,   as: :hidden,  input_html: { value: parent.resource.id }
              f.input :body,          label: false, input_html: { size: '80x8' }
              f.action :submit, label: I18n.t('active_admin.comments.add')

            end
          end
        end

        def default_id_for_prefix
          'active_admin_comments_for'
        end
      end

    end
  end
end
