module ActiveAdmin
  module Views
    class Header < Component

      def tag_name
        'nav'
      end
      def build(namespace, menu)
        super(class: 'navbar-default navbar-static-side', role: 'navigation' )

        @namespace = namespace
        @menu = menu
        @utility_menu = @namespace.fetch_menu(:utility_navigation)
        div class: 'sidebar-collapse' do
          build_global_navigation
        end
      end


      def build_global_navigation
        insert_tag view_factory.global_navigation, @menu, class: 'nav'
      end

      def build_utility_navigation
        insert_tag view_factory.utility_navigation, @utility_menu, id: "utility_nav", class: 'header-item tabs'
      end

    end
  end
end
