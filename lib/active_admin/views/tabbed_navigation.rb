module ActiveAdmin
  module Views

    # Renders an ActiveAdmin::Menu as a set of unordered list items.
    #
    # This component takes cares of deciding which items should be
    # displayed given the current context and renders them appropriately.
    #
    # The entire component is rendered within one ul element.
    class TabbedNavigation < Component

      attr_reader :menu

      # Build a new tabbed navigation component.
      #
      # @param [ActiveAdmin::Menu] menu the Menu to render
      # @param [Hash] options the options as passed to the underlying ul element.
      #
      def build(menu, options = {})
        @menu = menu
        super(default_options.merge(options))
        build_menu
      end

      # The top-level menu items that should be displayed.
      def menu_items
        menu.items(self)
      end

      def tag_name
        'ul'
      end

      private

      def build_menu
        li class: "nav-header" ,style: "padding: 10px;" do 
          div class: "profile-element text-center" do 
            span do
              image_tag current_admin_user.photo.url(:thumb) , class: "img-circle circle-border", width: "90px;" do 
              end
            end
            a href: admin_admin_user_path(current_admin_user) do 
              span class: :clear do 
                span class: "block m-t-xs" do 
                  strong current_admin_user.name, class: "font-bold"
                end
                span class: "text-muted text-xs block" do 
                  text_node current_admin_user.job 
                end
              end
            end
          end
          div class: "logo-element" do 
            "iLaw"
          end
        end
        menu_items.each do |item|
          build_menu_item(item)
        end
      end

      def build_menu_item(item, is_child = false)
        li id: item.id do |li|
          li.add_class "active" if item.current? assigns[:current_tab]

          if url = item.url(self)
              a href: url do 
                i class: item.icon(self)
                span  class:'nav-label' do 
                  text_node item.label(self)
                end 
                span class: 'fa arrow' if item.items(self).presence 

              end
          else
            span item.html_options do 
              i class: item.icon(self)
              text_node item.label(self)
            end 
          end

          if children = item.items(self).presence
            child_class = 'nav nav-second-level collapse'
            child_class << ' in' if item.current? assigns[:current_tab]
            ul class: child_class do
              children.each do  |child| 
                li class: child.current?(assigns[:current_tab]) ? "active": "" do 
                  link_to child.label(self), child.url(self)
                end
              end 
            end
          end
        end
      end

      def default_options
        { id: "side-menu", class: "nav" }
      end
    end
  end
end
