module ActiveAdmin
  module Views
    module Pages
      class Base < Arbre::HTML::Document

        def build(*args)
          super
          add_classes_to_body
          build_active_admin_head
          build_page
        end

        private

        def add_classes_to_body
          @body.add_class(params[:action])
          @body.add_class(params[:controller].tr('/', '_'))
          @body.add_class("active_admin")
          @body.add_class("logged_in")
          @body.add_class(active_admin_namespace.name.to_s + "_namespace")
        end

        def build_active_admin_head
          within @head do
            insert_tag Arbre::HTML::Title, [title, render_or_call_method_or_proc_on(self, active_admin_namespace.site_title)].compact.join(" | ")
            active_admin_application.stylesheets.each do |style, options|
              text_node stylesheet_link_tag(style, options).html_safe
            end

            active_admin_application.javascripts.each do |path|
              text_node(javascript_include_tag(path))
            end

            if active_admin_namespace.favicon
              text_node(favicon_link_tag(active_admin_namespace.favicon))
            end

            active_admin_namespace.meta_tags.each do |name, content|
              text_node(tag(:meta, name: name, content: content))
            end

            text_node csrf_meta_tag
          end
        end

        def build_page
          within @body do
            div id: "wrapper" do
              build_unsupported_browser
              build_header
             # build_title_bar
              build_page_content
            end
          end
        end

        def build_unsupported_browser
          if active_admin_namespace.unsupported_browser_matcher =~ request.user_agent
            insert_tag view_factory.unsupported_browser
          end
        end

        def build_header
          insert_tag view_factory.header, active_admin_namespace, current_menu
        end

        def build_title_bar
          insert_tag view_factory.title_bar, title, action_items_for_action
        end

        def build_page_content
          build_flash_messages
          div id: "page-wrapper", class: (skip_sidebar? ? "gray-bg without_sidebar" : "gray-bg with_sidebar") do
            build_top_row
            build_main_content_wrapper
            build_footer
          end
          build_sidebar 
        end

        def build_flash_messages
          div class: 'flashes' do
            flash_messages.each do |type, message|
              div message, class: "flash flash_#{type}"
            end
          end
        end

        def build_top_row
          div class: 'row border-bottom' do
            nav class: 'navbar navbar-static-top', role: 'navigation', style: "margin-bottom: 0" do
              div class: 'navbar-header' do
                a class: 'navbar-minimalize minimalize-styl-2 btn btn-primary' do
                  i class: 'fa fa-bars'
                end

                form action: "/admin/calendar/search", role: "search", class: "navbar-form-custom", method: "get" do |f|
                  div class: "form-group" do 
                    f.input type: "text", placeholder: "Бүх мэдээллээс хайх...", class: "form-control", name: "search", id: "top-search"
                  end 
                end
              end
              ul class: "nav navbar-top-links navbar-right" do 
                li do 
                  span class: "text-muted welcome-message"  do 
                    text_node "iOffice - т тавтай морил!"
                  end
                end

                li class: "dropdown" do 
                  a class: "dropdown-toggle count-info activity-notification", href: "#", "data-toggle": "dropdown" do 
                    i class: "fa fa-rss"
                    span "0", class: "label label-warning", id: "label-activity"
                  end
                  build_activity_content
                end
                li class: "dropdown" do 
                  a class: "dropdown-toggle count-info reminder-notification", href: "#","data-toggle": "dropdown"  do 
                    i class: "fa fa-bell"
                    span "0", class: "label label-primary", id: "label-reminder"
                  end
                  build_reminder_content
                end

                # li class: "project-people" do 
                #   a href: admin_admin_user_path(current_active_admin_user) do 
                #     if current_active_admin_user.photo.present?   
                #       img src: current_active_admin_user.photo.url(:small), class:"img-circle m-r-sm"
                #     else
                #       i class: "fa fa-user"
                #     end
                #   end 
                # end
                li do 
                  a href: "/admin/logout" do 
                    i class: "fa fa-sign-out"
                    text_node "Гарах"
                  end
                end
              end
            end
          end
        end

        def build_site_title
          insert_tag view_factory.site_title, title
        end

        def build_main_content_wrapper
          div class: "row wrapper border-bottom white-bg page-heading" do
            div class: "col-lg-6" do
              h2 title #title
            end
            div class: 'col-lg-6 right_bar' do
              build_action_items
            end
          end
          div class: "wrapper wrapper-content" do
            div class: 'row' do
              if skip_sidebar?
                main_content
              else
                # div class: "col-sm-12" do 
                #   build_sidebar
                # end
                div class: 'col-lg-12' do
                  main_content
                end
              end
            end
          end
        end

        def main_content
          I18n.t('active_admin.main_content', model: title).html_safe
        end

        def title
          self.class.name
        end

        # Set's the page title for the layout to render
        def set_page_title
          set_ivar_on_view "@page_title", title
        end

        # Returns the sidebar sections to render for the current action
        def sidebar_sections_for_action
          if active_admin_config && active_admin_config.sidebar_sections?
            active_admin_config.sidebar_sections_for(params[:action], self)
          else
            []
          end
        end

        def action_items_for_action
          if active_admin_config && active_admin_config.action_items?
            active_admin_config.action_items_for(params[:action], self)
          else
            []
          end
        end

        # Renders the sidebar
        def build_sidebar
          div id: "right-sidebar" do
            div class: "sidebar-container" do 
              sidebar_sections_for_action.collect do |section|
                sidebar_section(section)
              end
              div class: "hr-line-dashed"
              a class: "right-sidebar-toggle btn btn-primary pull-right m-r m-b" do 
                i class: "fa fa-forward"
              end
            end
          end
        end

        def skip_sidebar?
          sidebar_sections_for_action.empty? || assigns[:skip_sidebar] == true
        end

        # Renders the content for the footer
        def build_footer
          insert_tag view_factory.footer
        end

        def build_action_items
          insert_tag(view_factory.action_items, action_items_for_action)
        end
        def build_activity_content
          ul class: "dropdown-menu dropdown-alerts" do 
            li do 
              div class: "dropdown-messages-box" do 
                a href: "#" do 
                  div do 
                    text_node "You have 16"
                  end
                end
              end
            end
          end
        end
        def build_reminder_content
        end
      end
    end
  end
end
