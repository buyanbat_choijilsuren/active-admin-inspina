module ActiveAdmin
  module Views
    class Footer < Component

      def build
        super id: "footer", class: "footer"
        powered_by_message
      end

      private

      def powered_by_message
        div do 
          strong "Copyright"
          text_node "iSupport LLC © 2016 "
          text_node link_to "www.isupport.mn", "http://www.isupport.mn", target: "_blank"
        end
      end
    end
  end
end
