module ActiveAdmin
  module Views
    class Tabs < ActiveAdmin::Component
      builder_method :tabs

      def tab(title, options = {}, &block)
        id = (0...10).map { ('a'..'z').to_a[rand(26)] }.join

        title = title.to_s.titleize if title.is_a? Symbol
        @menu << build_menu_item(title, options, id,  &block)
        @tabs_content << build_content_item(title, options, id, &block)
      end

      def build(&block)
        @menu = ul(class: 'nav nav-tabs')
        @tabs_content = div(class: 'tab-content')
      end

      def build_menu_item(title, options, id, &block)
        options = options.reverse_merge({})
        options["data-toggle"] = "tab"
        if options[:class] == "active"
          li(class: "active") { link_to title, "##{id.parameterize}", options } 
        else
          li { link_to title, "##{id.parameterize}", options }
        end
      end

      def build_content_item(title, options, id, &block)
        options = options.reverse_merge(id: id.parameterize)
        if options[:class]
          options[:class] << " tab-pane" 
        else
          options[:class] = "tab-pane"
        end
        div(options, &block)
      end
    end
  end
end
